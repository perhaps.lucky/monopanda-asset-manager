﻿using MonoPanda.Configuration;

using Myra.Graphics2D.UI;
using Myra.Graphics2D.UI.File;

using System;
using System.Diagnostics;

namespace AssetManager {
  partial class UI {

    private void initializeOpenProjectFlow() {
      OpenProjectButton.Click += openProjectFlow_openFileDialog;
    }

    private void openProjectFlow_openFileDialog(object sender, EventArgs eventArgs) {
      var dialog = new FileDialog(FileDialogMode.OpenFile);
      dialog.Title = "Open asset json file";
      dialog.Filter = "*.json";
      dialog.Closed += openProjectFlow_fileDialogClosed;
      openProjectFlow_setLastPath(dialog);
      dialog.ShowModal(Desktop);
    }

    private void openProjectFlow_fileDialogClosed(object sender, EventArgs eventArgs) {
      var dialog = sender as FileDialog;
      openProjectFlow_saveLastPath(dialog.FilePath);
      if (dialog.Result) {
        openProjectFlow_tryOpenProject(dialog.FilePath);
      }
    }

    private void openProjectFlow_setLastPath(FileDialog dialog) {
      var valueFromConfig = Config.Get<AssetManagerConfig>().OpenProjectLastPath;
      if (!string.IsNullOrEmpty(valueFromConfig)) {
        dialog.FilePath = valueFromConfig;
      }
    }

    private void openProjectFlow_saveLastPath(string lastPath) {
      if (!string.IsNullOrEmpty(lastPath)) {
        Config.Get<AssetManagerConfig>().OpenProjectLastPath = lastPath;
        Config.SaveSettings();
      }
    }

    private void openProjectFlow_tryOpenProject(string filePath) {
      try {
        Project.OpenProject(filePath);
        refreshAssetList();
      } catch {
        Debugger.Break();
        openProjectFlow_showError();
      }
    }

    private void openProjectFlow_showError() {
      var errorDialog = new Window();
      errorDialog.Title = "Error while parsing file";
      errorDialog.ShowModal(Desktop);
    }

  }
}