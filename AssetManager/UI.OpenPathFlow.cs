﻿using AssetManager.AssetTypeHandling;
using AssetManager.ProcessMessages;

using Myra.Graphics2D.UI;
using Myra.Graphics2D.UI.File;

using System;
using System.IO;

namespace AssetManager {
  partial class UI {
    private void openAssetFileWindow(object sender, EventArgs eventArgs) {
      var openButton = sender as TextButton;
      var window = new FileDialog(FileDialogMode.OpenFile);
      window.FilePath = openPathFlow_getPathOrModDirectory(openButton);
      window.Filter = getPatternFromAsset(openButton);
      window.Tag = openButton;
      window.Closed += openPathFlow_windowClosed;
      window.ShowModal(Desktop);
    }

    private void openPathFlow_windowClosed(object sender, EventArgs eventArgs) {
      var window = sender as FileDialog;
      if (window.Result) {
        if (isPathInModFolder(window.FilePath)) {
          var pathField = (window.Tag as TextButton).Tag as TextBox;
          pathField.Text = window.FilePath.Replace(Project.GetModDirectory(), "");
          openPathFlow_importPaths(window.FilePath, pathField.Tag as string);
        } else {
          showPathNotInModDirectoryError();
        }
      }
    }

    private void openPathFlow_importPaths(string mainPath, string mainFieldId) {
      var messageBox = new MessageBox();
      var asset = AssetListBox.SelectedItem.Tag as ProjectAsset;
      var openDirectory = mainPath.Substring(0, mainPath.LastIndexOf('\\'));
      var fileName = mainPath.Substring(mainPath.LastIndexOf('\\') + 1).Split('.')[0];
      foreach (var pathField in AssetTypeCache.GetFields(asset.Type)) {
        if (pathField.FieldId == mainFieldId) {
          continue;
        }

        var importFileName = pathField.ImportPattern.Replace("*", fileName);
        var fullPath = $"{openDirectory}\\{importFileName}";
        if (File.Exists(fullPath)) {
          messageBox.Post(MessageType.AdditionalImport, $"{pathField.FieldId}|{fullPath}");
        } else if (pathField.Optional) {
          messageBox.Post(MessageType.OptionalImport, $"{pathField.FieldId}|{fullPath}");
        }
      }

      openPathFlow_showImportPathsWindow(messageBox);
    }

    private void showPathNotInModDirectoryError() {
      var errorDialog = new Window();
      errorDialog.Title = "Path must be within mod directory!";
      errorDialog.ShowModal(Desktop);
    }

    private bool isPathInModFolder(string path) {
      return path.StartsWith(Project.GetModDirectory());
    }

    private string getPatternFromAsset(TextButton button) {
      var pathField = button.Tag as TextBox;
      var asset = AssetListBox.SelectedItem.Tag as ProjectAsset;
      var fieldId = pathField.Tag as string;
      return AssetTypeCache
        .GetFields(asset.Type)
        .Find(f => f.FieldId == fieldId)
        .Pattern;
    }

    private string openPathFlow_getPathOrModDirectory(TextButton textButton) {
      var pathField = textButton.Tag as TextBox;
      if (string.IsNullOrEmpty(pathField.Text) || !Project.FileExists(pathField.Text)) {
        return Project.GetModDirectory();
      }

      return $"{Project.GetModDirectory()}\\{pathField.Text}";
    }
  }
}