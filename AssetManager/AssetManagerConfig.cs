﻿using MonoPanda.Configuration;
using MonoPanda.Configuration.Attributes;

namespace AssetManager {
  public class AssetManagerConfig : ConfigBase {
    
    [UserSettings]
    public string OpenProjectLastPath { get; set; }
    
    [UserSettings]
    public string ImportAssetLastPath { get; set; }
    
    public AssetManagerConfig() : base("AssetManager")
    {
    }
  }
}