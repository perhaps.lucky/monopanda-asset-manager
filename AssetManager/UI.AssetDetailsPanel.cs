﻿using AssetManager.AssetTypeHandling;

using FontStashSharp.RichText;

using Myra.Events;
using Myra.Graphics2D;
using Myra.Graphics2D.UI;

using System;
using System.Linq;

using TextBox = Myra.Graphics2D.UI.TextBox;

namespace AssetManager {
  partial class UI {
    private void initializeAssetDetailsPanel() {
      AssetDetailsPanel.Visible = false;
      AssetListBox.SelectedIndexChanged += assetDetailsPanel_selectedAssetChanged;
      AssetId.TextChanged += assetDetailsPanel_assetIdvalueChanged;
      NeverUnloadCheckbox.PressedChanged += assetDetailsPanel_neverUnloadValueChanged;
      assetDetailsPanel_initializeTypeList();
      AssetItemType.SelectedIndexChanged += assetDetailsPanel_assetItemTypeValueChange;
      assetDetailsPanel_clearPathFields();
    }

    private void assetDetailsPanel_initializeTypeList() {
      AssetItemType.Items.Clear();
      foreach (var assetType in AssetTypeCache.GetAssetTypes()) {
        AssetItemType.Items.Add(new ListItem {Text = assetType});
      }
    }

    private void assetDetailsPanel_selectedAssetChanged(object sender, EventArgs eventArgs) {
      if (AssetListBox.SelectedItem != null) {
        AssetDetailsPanel.Visible = true;
        assetDetailsPanel_loadAsset(AssetListBox.SelectedItem.Tag as ProjectAsset);
        AllGoodBox.Visible = false;
        loadInfoBox(AssetListBox.SelectedItem.Tag as ProjectAsset);
      } else {
        AssetDetailsPanel.Visible = false;
      }
    }

    private void assetDetailsPanel_loadAsset(ProjectAsset asset) {
      AssetId.Text = asset.Id;
      AssetItemType.SelectedItem = assetDetailsPage_getAssetTypeListItemFromAssetType(asset.Type);
      assetDetailsPanel_loadAssetPathValues();
      NeverUnloadCheckbox.IsChecked = asset.NeverUnload;
    }

    private void assetDetailsPanel_loadAssetPathValues() {
      var asset = AssetListBox.SelectedItem.Tag as ProjectAsset;
      foreach (var childrenWidget in PathTextBoxes.GetChildren(false, w => true)) {
        var pathTextBox = childrenWidget as TextBox;
        var id = pathTextBox.Tag as string;
        if (asset.FilePaths.ContainsKey(id)) {
          pathTextBox.Text = asset.FilePaths[id];
        }
      }
    }

    private void assetDetailsPanel_assetIdvalueChanged(object sender, ValueChangedEventArgs<string> eventArgs) {
      var asset = AssetListBox.SelectedItem.Tag as ProjectAsset;
      if (asset.Id == eventArgs.NewValue) {
        return;
      }

      asset.Id = AssetId.Text;
      AssetListBox.SelectedItem.Text = AssetId.Text;
      Project.ProjectChangeMade();
    }

    private void assetDetailsPanel_neverUnloadValueChanged(object sender, EventArgs eventArgs) {
      var asset = AssetListBox.SelectedItem.Tag as ProjectAsset;
      if (asset.NeverUnload == NeverUnloadCheckbox.IsChecked) {
        return;
      }

      asset.NeverUnload = NeverUnloadCheckbox.IsChecked;
      Project.ProjectChangeMade();
    }

    private ListItem assetDetailsPage_getAssetTypeListItemFromAssetType(string id) {
      try {
        return AssetItemType.Items.First(i => i.Text == id);
      } catch (InvalidOperationException) {
        return null;
      }
    }

    private void assetDetailsPanel_assetItemTypeValueChange(object sender, EventArgs eventArgs) {
      var asset = AssetListBox.SelectedItem.Tag as ProjectAsset;
      if (AssetItemType.SelectedItem != null && asset.Type != AssetItemType.SelectedItem.Text) {
        asset.Type = AssetItemType.SelectedItem.Text;
        Project.ProjectChangeMade();
      }

      assetDetailsPanel_typeChanged();
      assetDetailsPanel_loadAssetPathValues();
    }

    private void assetDetailsPanel_typeChanged() {
      assetDetailsPanel_clearPathFields();
      if (AssetItemType.SelectedItem != null) {
        assetDetailsPanel_addPathFields();
      }
    }

    private void assetDetailsPanel_clearPathFields() {
      removeChildren(PathLabels);
      removeChildren(PathTextBoxes);
      removeChildren(PathButtons);
    }

    private void assetDetailsPanel_addPathFields() {
      foreach (var path in AssetTypeCache.GetFields(AssetItemType.SelectedItem.Text)) {
        PathLabels.AddChild(new Label {Text = $"{path.Label}:"});
        var textBox = new TextBox {Tag = path.FieldId, MaxWidth = 250};
        textBox.TextChanged += assetDetailsPanel_pathValueChanged;
        PathTextBoxes.AddChild(textBox);
        PathButtons.AddChild(assetDetailsPanel_preparePathButtons(textBox));
      }
    }

    private HorizontalStackPanel assetDetailsPanel_preparePathButtons(TextBox textBox) {
      var panel = new HorizontalStackPanel {Spacing = 5};
      var openButton = new TextButton {Text = "Open", Padding = new Thickness(5, 0), Tag = textBox};
      openButton.Click += openAssetFileWindow;
      panel.AddChild(openButton);
      var importButton = new TextButton {Text = "Import", Padding = new Thickness(5, 0), Tag = textBox};
      importButton.Click += importAssetFileWindow;
      panel.AddChild(importButton);
      return panel;
    }

    private void assetDetailsPanel_pathValueChanged(object sender, EventArgs args) {
      var textBox = sender as TextBox;
      var id = textBox.Tag as string;
      var asset = AssetListBox.SelectedItem.Tag as ProjectAsset;

      if (asset.FilePaths.ContainsKey(id) && asset.FilePaths[id] == textBox.Text) {
        return;
      }

      asset.FilePaths[id] = textBox.Text;
      Project.ProjectChangeMade();
    }

    private void removeChildren(Widget panel) {
      foreach (var child in panel.GetChildren(false, w => true)) {
        child.RemoveFromParent();
      }
    }
  }
}