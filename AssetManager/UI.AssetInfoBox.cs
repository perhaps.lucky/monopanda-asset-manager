﻿using System;
using System.Linq;

namespace AssetManager {
  partial class UI {
    private void initializeInfoBox() {
      AllGoodBox.Visible = false;
      ErrorBox.Visible = false;
      WarnBox.Visible = false;
      Validate.Click += validateInfoBox;
    }

    private void validateInfoBox(object sender, EventArgs eventArgs) {
      var asset = AssetListBox.SelectedItem.Tag as ProjectAsset;
      asset.Validate();
      loadInfoBox(asset);
      if (!asset.HasErrors() && !asset.HasWarnings()) {
        AllGoodBox.Visible = true;
      }
      setAssetListValidationStatuses();
    }

    private void loadInfoBox(ProjectAsset asset) {
      // if (!asset.HasErrors() && !asset.HasWarnings()) {
      //   return;
      // }

      AllGoodBox.Visible = false;
      ErrorBox.Text = string.Join('\n', asset.Errors);
      WarnBox.Text = string.Join('\n', asset.Warns);
      ErrorBox.Visible = asset.HasErrors();
      WarnBox.Visible = asset.HasWarnings();
    }
  }
}