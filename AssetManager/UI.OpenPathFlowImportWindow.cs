﻿using AssetManager.ProcessMessages;

using MonoPanda.Extensions;

using Myra.Graphics2D;
using Myra.Graphics2D.UI;

using System.Collections.Generic;

namespace AssetManager {
  partial class UI {
    private void openPathFlow_showImportPathsWindow(MessageBox messageBox) {
      if (messageBox.IsEmpty()) {
        return;
      }

      var window = openPathFlow_importPaths_prepareWindow(out var verticalPanel);
      var importFileCheckboxes = openPathFlow_importPaths_prepareCheckBoxes_existingFiles(verticalPanel, messageBox);
      var optionalPathsCheckboxes = openPathFlow_importPaths_prepareCheckBoxes_optionalPaths(verticalPanel, messageBox);
      verticalPanel.AddChild(
        openPathFlow_importPaths_prepareButtons(window, importFileCheckboxes, optionalPathsCheckboxes));

      window.ShowModal(Desktop);
    }

    private VerticalStackPanel openPathFlow_importPaths_prepareCheckBoxes_existingFiles(VerticalStackPanel panel,
      MessageBox messageBox) {
      var messages = messageBox.Open().FindAll(m => m.MessageType == MessageType.AdditionalImport);
      if (messages.IsEmpty()) {
        return null;
      }

      panel.AddChild(new Label {Text = "Found matching files for these paths:"});
      return openPathFlow_importPaths_prepareCheckboxList(panel, messages);
    }

    private VerticalStackPanel openPathFlow_importPaths_prepareCheckBoxes_optionalPaths(VerticalStackPanel panel,
      MessageBox messageBox) {
      var messages = messageBox.Open().FindAll(m => m.MessageType == MessageType.OptionalImport);
      if (messages.IsEmpty()) {
        return null;
      }

      panel.AddChild(new Label {
        Text = "These paths are optional (there are no files found but they can be used by other mods):"
      });
      return openPathFlow_importPaths_prepareCheckboxList(panel, messages);
    }

    private Window openPathFlow_importPaths_prepareWindow(out VerticalStackPanel panel) {
      var window = new Window();
      window.CloseButton.Visible = false;
      window.Title = "Import other paths";
      panel = new VerticalStackPanel {Spacing = 5};
      window.Content = panel;
      return window;
    }

    private VerticalStackPanel openPathFlow_importPaths_prepareCheckboxList(VerticalStackPanel panel,
      List<Message> messages) {
      var checkBoxes = new VerticalStackPanel();
      foreach (var message in messages) {
        var split = message.MessageValue.Split('|');
        checkBoxes.AddChild(new CheckBox {IsChecked = true, Text = split[1], Tag = split[0]});
      }

      panel.AddChild(checkBoxes);
      return checkBoxes;
    }

    private HorizontalStackPanel openPathFlow_importPaths_prepareButtons(Window window,
      VerticalStackPanel importFileCheckBoxes, VerticalStackPanel optionalPathCheckBoxes) {
      var buttons = new HorizontalStackPanel {Spacing = 5};

      var cancelButton = new TextButton {Text = "Cancel", Padding = new Thickness(5)};
      cancelButton.Click += (sender, args) => window.Close();
      buttons.AddChild(cancelButton);

      var importButton = new TextButton {Text = "Import", Padding = new Thickness(5)};
      importButton.Click += (sender, args) =>
        openPathFlow_importPaths_importCheckedFiles(importFileCheckBoxes, optionalPathCheckBoxes, window);
      buttons.AddChild(importButton);

      return buttons;
    }

    private void openPathFlow_importPaths_importCheckedFiles(VerticalStackPanel importFileCheckBoxes,
      VerticalStackPanel optionalPathCheckBoxes, Window importPathsDialog) {
      if (importFileCheckBoxes != null) {
        openPathFlow_importPaths_importCheckFilesFromPanel(importFileCheckBoxes);
      }

      if (optionalPathCheckBoxes != null) {
        openPathFlow_importPaths_importCheckFilesFromPanel(optionalPathCheckBoxes);
      }

      importPathsDialog.Close();
    }

    private void openPathFlow_importPaths_importCheckFilesFromPanel(VerticalStackPanel panel) {
      foreach (var widget in panel.GetChildren(false, w => (w as CheckBox).IsChecked)) {
        var pathToImport = widget as CheckBox;
        var textField = openPathFlow_importPaths_findTextField(widget.Tag as string);
        textField.Text = pathToImport.Text.Replace(Project.GetModDirectory(), "");
      }
    }

    private TextBox openPathFlow_importPaths_findTextField(string fieldId) {
      foreach (var field in PathTextBoxes.GetChildren(false, w => true)) {
        if (field.Tag as string == fieldId) {
          return field as TextBox;
        }
      }

      throw new KeyNotFoundException(); // shouldn't happen
    }
  }
}