﻿using AssetManager.ProcessMessages;

using Myra.Graphics2D;
using Myra.Graphics2D.UI;

namespace AssetManager {
  partial class UI {
    private void openFileDeletionDialog(MessageBox saveMessageBox) {
      if (saveMessageBox.IsEmpty()) {
        return;
      }

      var window = fileDeletionDialog_prepareWindow(out var verticalPanel);
      var checkBoxes = fileDeletionDialog_prepareCheckboxList(saveMessageBox);
      verticalPanel.AddChild(checkBoxes);
      verticalPanel.AddChild(fileDeletionDialog_prepareButtons(window, checkBoxes));

      window.ShowModal(Desktop);
    }

    private void fileDeletionDialog_removeCheckedFiles(VerticalStackPanel checkBoxes, Window deleteFileDialog) {
      foreach (var widget in checkBoxes.GetChildren(false, w => (w as CheckBox).IsChecked)) {
        var fileToRemove = widget as CheckBox;
        Project.RemoveFile(fileToRemove.Text);
      }

      deleteFileDialog.Close();
    }

    private Window fileDeletionDialog_prepareWindow(out VerticalStackPanel panel) {
      var window = new Window();
      window.CloseButton.Visible = false;
      window.Title = "These files might be no longer needed, do you want to remove them?";
      panel = new VerticalStackPanel {Spacing = 5};
      window.Content = panel;
      return window;
    }

    private VerticalStackPanel fileDeletionDialog_prepareCheckboxList(MessageBox messageBox) {
      var checkBoxes = new VerticalStackPanel();
      foreach (var message in messageBox.Open().FindAll(m => m.MessageType == MessageType.Save_FileExist)) {
        checkBoxes.AddChild(new CheckBox {IsChecked = true, Text = message.MessageValue});
      }

      return checkBoxes;
    }

    private HorizontalStackPanel fileDeletionDialog_prepareButtons(Window window, VerticalStackPanel checkBoxes) {
      var buttons = new HorizontalStackPanel {Spacing = 5};
      
      var cancelButton = new TextButton {Text = "Cancel", Padding = new Thickness(5)};
      cancelButton.Click += (sender, args) => window.Close();
      buttons.AddChild(cancelButton);

      var removeButton = new TextButton {Text = "Remove", Padding = new Thickness(5)};
      removeButton.Click += (sender, args) => fileDeletionDialog_removeCheckedFiles(checkBoxes, window);
      buttons.AddChild(removeButton);

      return buttons;
    }
  }
}