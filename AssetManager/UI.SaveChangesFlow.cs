﻿using AssetManager.ProcessMessages;

using Myra.Graphics2D;
using Myra.Graphics2D.UI;

using Spine;

using System;
using System.Diagnostics;
using System.Linq;

namespace AssetManager {
  partial class UI {
    private void initializeSaveButton() {
      SaveChangesButton.Enabled = false;
      Project.ProjectChangeMade += saveChangesFlow_enable;
      SaveChangesButton.Click += saveChangesFlow_save;
    }

    private void saveChangesFlow_enable() {
      SaveChangesButton.Enabled = true;
    }

    private void saveChangesFlow_save(object sender, EventArgs eventArgs) {
      try {
        var saveMessageBox = Project.SaveProject(PrettyPrintCheckbox.IsChecked);
        setAssetListValidationStatuses();
        SaveChangesButton.Enabled = false;
        openFileDeletionDialog(saveMessageBox);
        if (AssetListBox.SelectedItem != null) {
          loadInfoBox(AssetListBox.SelectedItem.Tag as ProjectAsset);
        }
      } catch {
        saveChangesFlow_showError();
      }
    }

    private void saveChangesFlow_showError() {
      var errorDialog = new Window();
      errorDialog.Title = "Error while saving file";
      errorDialog.ShowModal(Desktop);
    }
  }
}