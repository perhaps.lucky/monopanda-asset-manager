﻿using MonoPanda.Assets;
using MonoPanda.Assets.Attributes;
using MonoPanda.ClassScanning;

using System;
using System.Reflection;

namespace AssetManager.AssetTypeHandling {
  public class AssetTypeHandler : IClassHandler {
    public bool ShouldBeHandled(Type type) {
      return type.IsSubclassOf(typeof(Asset)) && type.GetCustomAttribute<AssetType>() != null;
    }

    public void Handle(Type type) {
      AssetTypeCache.Register(type);
    }

    public void InitHandler() {
      AssetTypeCache.Init();
    }
  }
}