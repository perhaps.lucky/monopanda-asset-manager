﻿using MonoPanda.Assets.Attributes;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;

namespace AssetManager.AssetTypeHandling {
  public class AssetTypeCache {
    private static Dictionary<string, List<PathField>> assetTypeToPathFields;

    public static void Init() {
      assetTypeToPathFields = new Dictionary<string, List<PathField>>();
    }

    public static void Register(Type assetType) {
      var fields = new List<PathField>();

      foreach (var assetPathPropertyInfo in getListOfAssetPaths(assetType)) {
        fields.Add(new PathField(assetPathPropertyInfo.Name,
          assetPathPropertyInfo.GetCustomAttribute(typeof(AssetPath)) as AssetPath));
      }

      var typeName = assetType.GetCustomAttribute<AssetType>().TypeName;
      assetTypeToPathFields.Add(typeName, fields);
    }

    public static List<PathField> GetFields(string assetType) {
      if (assetType == null) {
        return new List<PathField>();
      }
      return assetTypeToPathFields[assetType];
    }
    
    public static List<string> GetAssetTypes() {
      return assetTypeToPathFields.Keys.ToList();
    }

    private static IEnumerable<PropertyInfo> getListOfAssetPaths(Type assetType) {
      return assetType.GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.NonPublic)
        .Where(p => propertyHasAttribute(p, typeof(AssetPath)));
    }

    private static bool propertyHasAttribute(PropertyInfo propertyInfo, Type attributeType) {
      return propertyInfo.GetCustomAttributes(attributeType, true).Any();
    }
  }
}