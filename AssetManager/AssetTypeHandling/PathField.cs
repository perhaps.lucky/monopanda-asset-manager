﻿using MonoPanda.Assets.Attributes;

namespace AssetManager.AssetTypeHandling {
  public class PathField {
    public string FieldId { get; private set; }
    public string Label { get; private set; }
    public string Pattern { get; private set; }
    
    public string ImportPattern { get; private set; }
    public bool Optional { get; private set; }
    
    public PathField(string fieldId, AssetPath assetPathAttribute) {
      FieldId = fieldId;
      Label = assetPathAttribute.Label;
      Pattern = assetPathAttribute.FilePattern;
      Optional = assetPathAttribute.Optional;
      ImportPattern = assetPathAttribute.ImportFilePattern ?? Pattern;

    }
    
  }
}