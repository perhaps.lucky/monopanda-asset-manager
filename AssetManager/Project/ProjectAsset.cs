﻿using AssetManager.AssetTypeHandling;
using AssetManager.ProcessMessages;

using MonoPanda.Extensions;

using Newtonsoft.Json.Linq;

using System;
using System.Collections.Generic;
using System.Linq;

namespace AssetManager {
  public class ProjectAsset {
    public string Id { get; set; }
    public string Type { get; set; }
    public bool NeverUnload { get; set; }
    public Dictionary<string, string> FilePaths { get; }

    public List<string> Errors { get; }

    public List<string> Warns { get; }

    public ProjectAsset() {
      FilePaths = new Dictionary<string, string>();
      Errors = new List<string>();
      Warns = new List<string>();
    }

    public JObject ToJson(MessageBox messageBox) {
      var json = new JObject();
      json.Add("Id", Id);
      json.Add("Type", Type);
      json.Add("NeverUnload", NeverUnload);
      foreach (var keyValuePair in FilePaths) {
        if (isPathPartOfType(keyValuePair.Key)) {
          json.Add(keyValuePair.Key, keyValuePair.Value);
        } else {
          if (Project.FileExists(keyValuePair.Value)) {
            messageBox.Post(MessageType.Save_FileExist, keyValuePair.Value);
          }
        }
      }

      return json;
    }

    public void ClearWarnsAndErrors() {
      Warns.Clear();
      Errors.Clear();
    }

    public bool Validate() {
      Errors.Clear();
      Warns.Clear();
      
      if (String.IsNullOrEmpty(Id)) {
        Errors.Add("Id value can't be empty!");
      } else if (Project.CheckAssetId(Id, out var numberOfAsset)) {
        Errors.Add($"Found {numberOfAsset} asset with same Id! Id should be unique.");
      }

      if (String.IsNullOrEmpty(Type)) {
        Errors.Add("Type is not selected!");
      }

      if (Type != null) {
        AssetTypeCache
          .GetFields(Type)
          .Where(f => !f.Optional)
          .Select(f => f.FieldId)
          .ForEach(validatePathField);
      }
      
      return !HasErrors();
    }

    public bool HasErrors() {
      return Errors.IsNotEmpty();
    }

    public bool HasWarnings() {
      return Warns.IsNotEmpty();
    }

    private bool isPathPartOfType(string pathId) {
      return AssetTypeCache.GetFields(Type).Any(f => f.FieldId == pathId);
    }

    private void validatePathField(string fieldId) {
      if (!FilePaths.ContainsKey(fieldId) || string.IsNullOrEmpty(FilePaths[fieldId])) {
        Errors.Add($"Field: {fieldId} is empty");
      } else if (!Project.FileExists(FilePaths[fieldId])) {
        Warns.Add($"File: {FilePaths[fieldId]} does not exist.");
      }
    }
  }
}