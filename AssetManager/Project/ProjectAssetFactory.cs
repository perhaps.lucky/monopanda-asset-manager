﻿using AssetManager.AssetTypeHandling;

using Newtonsoft.Json.Linq;

using System.Linq;
using System.Text.Json.Nodes;

namespace AssetManager {
  public class ProjectAssetFactory {
    public static ProjectAsset FromJson(JObject json) {
      var asset = new ProjectAsset();
      asset.Id = json["Id"].Value<string>();
      asset.Type = json["Type"].Value<string>();
      asset.NeverUnload = json["NeverUnload"]?.Value<bool?>() ?? false;
      foreach (var pathId in AssetTypeCache.GetFields(asset.Type).Select(p => p.FieldId)) {
        asset.FilePaths.Add(pathId, json[pathId]?.Value<string>() ?? "");
      } 
      return asset;
    }

    public static ProjectAsset Empty() {
      var asset = new ProjectAsset();
      asset.Id = "new-asset";
      return asset;
    }
  }
}