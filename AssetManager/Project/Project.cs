﻿using AssetManager.ProcessMessages;

using MonoPanda.Utils;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using System;
using System.Collections.Generic;
using System.IO;

namespace AssetManager {
  public class Project {
    public static Action ProjectChangeMade;
    public static Action ProjectOpened;
    public static Action<ProjectAsset> AssetAdded;
    public static Action<ProjectAsset> AssetRemoved;

    private static Project loadedProject;

    private List<ProjectAsset> assets;
    private string filePath;
    private string directoryPath;

    public static void OpenProject(string path) {
      loadedProject = new Project(path);
      ProjectOpened?.Invoke();
    }

    public static List<ProjectAsset> GetAssets() {
      return loadedProject.assets;
    }

    public static void AddAsset() {
      var asset = ProjectAssetFactory.Empty();
      loadedProject.assets.Add(asset);
      AssetAdded?.Invoke(asset);
      ProjectChangeMade?.Invoke();
    }

    public static void RemoveAsset(ProjectAsset asset) {
      loadedProject.assets.Remove(asset);
      AssetRemoved?.Invoke(asset);
      ProjectChangeMade?.Invoke();
    }

    public static MessageBox SaveProject(bool prettyFormat) {
      var messageBox = new MessageBox();
      var jsonContent = new JArray();
      foreach (var asset in loadedProject.assets) {
        if (asset.Validate()) {
          jsonContent.Add(asset.ToJson(messageBox));
        }
      }

      File.WriteAllText(loadedProject.filePath, jsonContent.ToString(prettyFormat ? Formatting.Indented : Formatting.None));
      return messageBox;
    }

    public static bool FileExists(string path) {
      if (String.IsNullOrEmpty(path)) {
        return false;
      }

      return File.Exists($"{loadedProject.directoryPath}\\{path}");
    }

    public static void RemoveFile(string path) {
      if (!FileExists(path)) {
        return;
      }

      File.Delete($"{loadedProject.directoryPath}\\{path}");
    }

    public static bool CheckAssetId(string id, out int numberOfAssets) {
      numberOfAssets = loadedProject.assets.FindAll(a => a.Id == id).Count;
      return numberOfAssets > 1;
    }

    public static string GetModDirectory() {
      return $"{loadedProject.directoryPath}\\";
    }

    private Project(string path) {
      filePath = path;
      directoryPath = path.Substring(0, path.LastIndexOf("\\"));
      assets = new List<ProjectAsset>();
      var jsonContent = ContentUtils.LoadJson<JArray>(path);
      foreach (var jsonObject in jsonContent) {
        assets.Add(ProjectAssetFactory.FromJson((JObject)jsonObject));
      }
    }
  }
}