﻿namespace AssetManager.ProcessMessages {
  public enum MessageType {
    Save_FileExist, AdditionalImport, OptionalImport, Import_MainImport
  }
}