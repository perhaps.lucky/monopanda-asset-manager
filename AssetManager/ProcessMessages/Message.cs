﻿namespace AssetManager.ProcessMessages {
  public class Message {
    public MessageType MessageType { get; }
    public string MessageValue { get; }

    public Message(MessageType type, string value) {
      MessageType = type;
      MessageValue = value;
    }
  }
}