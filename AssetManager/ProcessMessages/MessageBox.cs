﻿using System.Collections.Generic;

namespace AssetManager.ProcessMessages {
  public class MessageBox {
    private List<Message> messages;

    public MessageBox() {
      messages = new List<Message>();
    }

    public void Post(MessageType messageType, string message) {
      messages.Add(new Message(messageType, message));
    }

    public List<Message> Open() {
      return messages;
    }

    public bool IsEmpty() {
      return messages.Count == 0;
    }
  }
}