﻿using AssetManager.ProcessMessages;

using System;
using System.Linq;

using MonoPanda.Extensions;

namespace AssetManager {
  partial class UI {
    private void initializeAssetListControlButtons() {
      AddAssetButton.Click += assetListControlButtons_addAsset;
      RemoveAssetButton.Click += assetListControlButtons_removeAsset;
      AddAssetButton.Enabled = false;
      RemoveAssetButton.Enabled = false;
      Project.ProjectOpened += assetListControlButtons_enableControls;
    }

    private void assetListControlButtons_addAsset(object sender, EventArgs eventArgs) {
      Project.AddAsset();
    }

    private void assetListControlButtons_removeAsset(object sender, EventArgs eventArgs) {
      if (AssetListBox.SelectedItem != null) {
        var asset = AssetListBox.SelectedItem.Tag as ProjectAsset;
        assetListControlButtons_checkPathsForRemoval(asset);
        Project.RemoveAsset(asset);
      }
    }

    private void assetListControlButtons_enableControls() {
      AddAssetButton.Enabled = true;
      RemoveAssetButton.Enabled = true;
    }

    private void assetListControlButtons_checkPathsForRemoval(ProjectAsset asset) {
      var messageBox = new MessageBox();
      asset.FilePaths.Values
        .Where(path => Project.FileExists(path))
        .ForEach(existingFilePath => messageBox.Post(MessageType.Save_FileExist, existingFilePath));
      openFileDeletionDialog(messageBox);
    }
  }
}