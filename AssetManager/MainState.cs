﻿using MonoPanda;
using MonoPanda.States;

using Myra;
using Myra.Graphics2D.UI;

namespace AssetManager {
  public class MainState : State {

    private Desktop myraDesktop;
    private UI myraUi;
    
    public MainState(string id) : base(id)
    {
    }

    public override void Initialize() {
      MyraEnvironment.Game = GameMain.Instance;
      myraDesktop = new Desktop();
      myraUi = new UI();
      myraDesktop.Widgets.Add(myraUi);
    }

    public override void Draw() {
      if (GameMain.Instance.IsActive) { // TODO: remove after version with https://github.com/rds1983/Myra/pull/449 or other fix
        myraDesktop.Render();
      }
    }
  }
}