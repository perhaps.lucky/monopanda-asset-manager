﻿using System.IO;
using System.Reflection;

namespace AssetManager {
  public class DllLoader {
    private static readonly string DLL_DIRECTORY = "Content/Dlls";

    public static void LoadExternalDlls() {
      foreach (var dllFile in Directory.GetFiles(DLL_DIRECTORY, "*.dll")) {
        Assembly.LoadFrom(dllFile);
      }
    }
  }
}