﻿using Microsoft.Xna.Framework;

using Myra.Graphics2D.UI;

using System.Linq;

namespace AssetManager {
  partial class UI {
    private void refreshAssetList() {
      AssetListBox.Items.Clear();
      Project.GetAssets().ForEach(assetList_addAssetItem);
      Project.AssetAdded += assetList_addAssetItem;
      Project.AssetRemoved += assetList_removeAssetItem;
    }

    private void initializeAssetList() {
      AssetListBox.Items.Clear();
    }

    private void setAssetListValidationStatuses() {
      foreach (var item in AssetListBox.Items) {
        var asset = item.Tag as ProjectAsset;
        
        
        if (asset.HasErrors()) {
          item.Color = Color.Red;
        } else if (asset.HasWarnings()) {
          item.Color = Color.Yellow;
        } else {
          item.Color = Color.White;
        }
      }
    }

    private void assetList_addAssetItem(ProjectAsset asset) {
      var item = new ListItem{
        Text = asset.Id,
        Tag = asset
      };
      AssetListBox.Items.Add(item);
    }

    private void assetList_removeAssetItem(ProjectAsset asset) {
      var item = AssetListBox.Items.First(item => item.Tag == asset);
      if (item != null) {
        AssetListBox.Items.Remove(item);
        if (AssetListBox.SelectedItem == item) {
          AssetListBox.SelectedItem = null;
        }
      }
    }
  }
}