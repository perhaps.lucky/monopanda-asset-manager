﻿using AssetManager.AssetTypeHandling;
using AssetManager.ProcessMessages;

using MonoPanda;
using MonoPanda.Configuration;
using MonoPanda.Extensions;

using Myra.Graphics2D;
using Myra.Graphics2D.UI;
using Myra.Graphics2D.UI.File;

using System;
using System.IO;
using System.Linq;

namespace AssetManager {
  partial class UI {
    public void importAssetFileWindow(object sender, EventArgs eventArgs) {
      var importButton = sender as TextButton;
      var window = new FileDialog(FileDialogMode.OpenFile);
      importAssetFlow_setLastPath(window);
      window.Filter = getPatternFromAsset(importButton);
      window.Tag = importButton;
      window.Closed += importAssetFlow_windowClosed;
      window.ShowModal(Desktop);
    }

    private void importAssetFlow_setLastPath(FileDialog dialog) {
      var valueFromConfig = Config.Get<AssetManagerConfig>().ImportAssetLastPath;
      if (!string.IsNullOrEmpty(valueFromConfig)) {
        dialog.FilePath = valueFromConfig;
      }
    }

    private void importAssetFlow_saveLastPath(string lastPath) {
      if (!string.IsNullOrEmpty(lastPath)) {
        Config.Get<AssetManagerConfig>().ImportAssetLastPath = lastPath;
        Config.SaveSettings();
      }
    }

    private void importAssetFlow_windowClosed(object sender, EventArgs eventArgs) {
      var window = sender as FileDialog;
      if (window.Result) {
        importAssetFlow_saveLastPath(window.FilePath);
        var saveAssetWindow = new FileDialog(FileDialogMode.ChooseFolder);
        saveAssetWindow.FilePath = Project.GetModDirectory();
        saveAssetWindow.Closed += (o, args) => importAssetFlow_saveAssetWindowClosed(saveAssetWindow, window);
        saveAssetWindow.ShowModal(Desktop);
      }
    }

    private void importAssetFlow_saveAssetWindowClosed(FileDialog saveAssetWindow, FileDialog selectFileWindow) {
      if (saveAssetWindow.Result) {
        if (isPathInModFolder(saveAssetWindow.FilePath)) {
          importAssetFlow_importFiles(selectFileWindow.FilePath, saveAssetWindow.FilePath,
            ((selectFileWindow.Tag as TextButton).Tag as TextBox).Tag as string);
        } else {
          showPathNotInModDirectoryError();
        }
      }
    }

    private void importAssetFlow_importFiles(string selectedFilePath, string saveLocation, string mainFieldId) {
      var messageBox = new MessageBox();
      var asset = AssetListBox.SelectedItem.Tag as ProjectAsset;
      var openDirectory = selectedFilePath.Substring(0, selectedFilePath.LastIndexOf('\\'));
      var fileName = selectedFilePath.Substring(selectedFilePath.LastIndexOf('\\') + 1).Split('.')[0];
      foreach (var pathField in AssetTypeCache.GetFields(asset.Type)) {
        var importFileName = pathField.ImportPattern.Replace("*", fileName);
        var fullPath = $"{openDirectory}\\{importFileName}";
        var message = $"{pathField.FieldId}|{fullPath}";

        if (pathField.FieldId == mainFieldId) {
          messageBox.Post(MessageType.Import_MainImport, message);
        } else if (File.Exists(fullPath)) {
          messageBox.Post(MessageType.AdditionalImport, message);
        } else if (pathField.Optional) {
          var optMessage =
            $"{pathField.FieldId}|{saveLocation.Replace(Project.GetModDirectory(), "")}\\{importFileName}";
          messageBox.Post(MessageType.OptionalImport, optMessage);
        }
      }

      var mainMsgSplit = messageBox.Open().Find(m => m.MessageType == MessageType.Import_MainImport).MessageValue
        .Split('|');
      importAssetFlow_importFile(mainMsgSplit[0], mainMsgSplit[1], saveLocation);
      importAssetFlow_showImportFilesWindow(messageBox, saveLocation);
    }

    private void importAssetFlow_showImportFilesWindow(MessageBox messageBox, string saveLocation) {
      if (messageBox.Open().FindAll(m => m.MessageType != MessageType.Import_MainImport).IsEmpty()) {
        return;
      }

      var window = importAssetFlow_importFiles_prepareWindow(out var verticalPanel);
      var importFileCheckboxes = openPathFlow_importPaths_prepareCheckBoxes_existingFiles(verticalPanel, messageBox);
      var optionalPathsCheckboxes = openPathFlow_importPaths_prepareCheckBoxes_optionalPaths(verticalPanel, messageBox);
      verticalPanel.AddChild(
        importAssetFlow_importFiles_prepareButtons(window, importFileCheckboxes, optionalPathsCheckboxes,
          saveLocation));

      window.ShowModal(Desktop);
    }

    private Window importAssetFlow_importFiles_prepareWindow(out VerticalStackPanel panel) {
      var window = new Window();
      window.CloseButton.Visible = false;
      window.Title = "Import additional files";
      panel = new VerticalStackPanel {Spacing = 5};
      window.Content = panel;
      return window;
    }

    private HorizontalStackPanel importAssetFlow_importFiles_prepareButtons(Window window,
      VerticalStackPanel importFileCheckBoxes, VerticalStackPanel optionalPathCheckBoxes, string saveLocation) {
      var buttons = new HorizontalStackPanel {Spacing = 5};

      var cancelButton = new TextButton {Text = "Cancel", Padding = new Thickness(5)};
      cancelButton.Click += (sender, args) => window.Close();
      buttons.AddChild(cancelButton);

      var importButton = new TextButton {Text = "Import", Padding = new Thickness(5)};
      importButton.Click += (sender, args) =>
        importAssetFlow_importFiles_importCheckedFiles(importFileCheckBoxes, optionalPathCheckBoxes, window,
          saveLocation);
      buttons.AddChild(importButton);

      return buttons;
    }

    private void importAssetFlow_importFiles_importCheckedFiles(VerticalStackPanel importFileCheckBoxes,
      VerticalStackPanel optionalPathCheckBoxes, Window importPathsDialog, string saveLocation) {
      if (importFileCheckBoxes != null) {
        importAssetFlow_importFiles_importCheckFilesFromPanel(importFileCheckBoxes, saveLocation);
      }

      if (optionalPathCheckBoxes != null) {
        // No file import, only paths like in open-flow
        foreach (var optionalPath in optionalPathCheckBoxes.GetChildren(false, w => (w as CheckBox).IsChecked)) {
          importAssetFlow_importFiles_optionalFieldCheckForOldFile(optionalPath as CheckBox);
        }

        openPathFlow_importPaths_importCheckFilesFromPanel(optionalPathCheckBoxes);
      }

      importPathsDialog.Close();
    }

    private void importAssetFlow_importFiles_optionalFieldCheckForOldFile(CheckBox checkBox) {
      var fieldId = checkBox.Tag as string;
      var previousValue = openPathFlow_importPaths_findTextField(fieldId).Text;
      var newValue = checkBox.Text;
      if (previousValue != newValue) {
        importAssetFlow_checkForOldFile(previousValue);
      }
    }

    private void importAssetFlow_importFiles_importCheckFilesFromPanel(VerticalStackPanel panel, string saveLocation) {
      foreach (var widget in panel.GetChildren(false, w => (w as CheckBox).IsChecked)) {
        var pathToImport = widget as CheckBox;
        importAssetFlow_importFile(pathToImport.Tag as string, pathToImport.Text, saveLocation);
      }
    }

    private void importAssetFlow_importFile(string fieldId, string sourceFile, string saveLocation) {
      var fileName = sourceFile.Substring(sourceFile.LastIndexOf('\\') + 1);
      var destFile = $"{saveLocation}\\{fileName}";
      if (File.Exists(destFile)) {
        importAssetFlow_fileExistsOverride(fieldId, sourceFile, destFile);
      } else {
        importAssetFlow_copyFileAndSave(fieldId, sourceFile, destFile);
      }
    }

    private void importAssetFlow_fileExistsOverride(string fieldId, string sourceFile, string destFile) {
      var window = new Window();
      window.CloseButton.Visible = false;
      window.Title = $"File already exists. Override?";
      var panel = new VerticalStackPanel {Spacing = 5};
      window.Content = panel;

      panel.AddChild(new Label {Text = destFile});

      var buttons = new HorizontalStackPanel {Spacing = 5};

      var cancelButton = new TextButton {Text = "Cancel", Padding = new Thickness(5)};
      cancelButton.Click += (sender, args) => window.Close();
      buttons.AddChild(cancelButton);

      var overrideButton = new TextButton {Text = "Override", Padding = new Thickness(5)};
      overrideButton.Click += (sender, args) => {
        window.Close();
        importAssetFlow_copyFileAndSave(fieldId, sourceFile, destFile);
      };
      buttons.AddChild(overrideButton);

      panel.AddChild(buttons);
      window.ShowModal(Desktop);
    }

    private void importAssetFlow_copyFileAndSave(string fieldId, string sourceFile, string destFile) {
      File.Copy(sourceFile, destFile, true);

      if (fieldId == null) {
        return;
      }
      
      var newFieldValue = $"{destFile.Replace(Project.GetModDirectory(), "")}";
      var textField = openPathFlow_importPaths_findTextField(fieldId);
      if (textField.Text != newFieldValue) {
        importAssetFlow_checkForOldFile(textField.Text);
      }

      textField.Text = newFieldValue;
    }

    private void importAssetFlow_checkForOldFile(string path) {
      if (Project.FileExists(path)) {
        var destFile = $"{Project.GetModDirectory()}\\{path}";
        var window = new Window();
        window.CloseButton.Visible = false;
        window.Title = $"Do you want to remove old file?";
        var panel = new VerticalStackPanel {Spacing = 5};
        window.Content = panel;

        panel.AddChild(new Label {Text = destFile});

        var buttons = new HorizontalStackPanel {Spacing = 5};

        var cancelButton = new TextButton {Text = "Cancel", Padding = new Thickness(5)};
        cancelButton.Click += (sender, args) => window.Close();
        buttons.AddChild(cancelButton);

        var removeButton = new TextButton {Text = "Remove", Padding = new Thickness(5)};
        removeButton.Click += (sender, args) => {
          window.Close();
          File.Delete(destFile);
        };
        buttons.AddChild(removeButton);

        panel.AddChild(buttons);
        window.ShowModal(Desktop);
      }
    }
  }
}