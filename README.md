# MonoPanda-AssetManager

## About

MonoPanda-AssetManager is a simple program dedicated to managing asset in MonoPanda projects (games or mods). This program not only able to manage content of assets list file, but also add and remove files from mod directory. Note that while changes on asset list file are saved only after pressing save, changes on mod directory are instant, **so ALWAYS use some sort of version control** - in case you are not doing it already.

## Download & compability

Check release section for newest version.

AssetManager is a part of MonoPanda 2.0+ and as such, should be compatible with newest version of MonoPanda.

## How to use

### Opening project

Currently there is no way to create new project - please create assets file manually (content should be an empty list).

### List of assets

On the left side, there is a list of assets. There you can add and remove your assets. While removing asset, you might be asked if you want to remove files that were set in asset's paths.

### Asset details

On the right side, you can set parameters of selected asset. Each asset will have parameters:

* **Id** - unique id of asset
* **Never unload** - if checkbox is checked, then once asset is loaded, it will never be automatically unloaded
* **Item type** - type of asset - depending on selected type, additional path fields will show up

Path fields values are paths to file relative to mod directory. There are two options to automatically set them: `Open` and `Import`

### Open asset file

Using `Open` option, you will be asked to select file that is **already in the mod directory**. 

Based on patterns of other fields, if there are other files with same name (but different extension), you might be asked if you want to automatically fill out other paths of asset.

### Import asset file

Using `Import` option, you will first be asked to select file that you want to **copy into the mod directory**, then a second window will ask you for location within mod directory where file should be pasted. Program will copy file and set path field value automatically.

Similar to `Open` option, this might also ask you if you want to import additional files. All files will be copied.

Additionally, if import is used when there is already an existing file for path, program will ask if you want to delete the old file.

### Saving

While saving, all assets will be validated. If asset name on the list is red, then there is an error and that asset will not be saved. **Note that it will be not saved at all, so it will only remain in program memory unless fixed and saved again**.

You can always validate any asset by pressing `Validate` button in details section.

You can check `Pretty` option next to save button if you want to save your assets list in pretty formating.

You might be asked if you want to delete some files - these files are asset paths that are not matching the type of asset. This will happen for example when you import some file, but then change asset type to different one.

### Custom asset types

If your project uses custom asset types, you can load them into AssetManager by adding your project .dll file into `/Content/Dlls/` directory. Your type will be loaded on startup of AssetManager and added to the list.
